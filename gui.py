#!/bin/env python3

import os
import sys

from PySide2 import QtCore, QtWidgets
from serial import SerialException

from ArduinoRobot import RobotCommunicator, RobotPlanner, PathType
from guiclasses import BetterComboBox, JointAngleSlider, MplCanvas

if os.name == 'nt':  # sys.platform == 'win32':
    from serial.tools.list_ports_windows import comports
elif os.name == 'posix':
    from serial.tools.list_ports_posix import comports


class MainWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        self.app_started = False
        QtWidgets.QWidget.__init__(self, parent)

        self.__default_num_joints = 1
        self.__default_steps = 250
        self.__default_table_data = [
            [0.0, -90.0],
            [0.9, 90.0],
            [1.0, 0.0],
            [1.0, 25.0],
        ]

        self.robo_communicator = RobotCommunicator()
        self.robo_planner = RobotPlanner(5)
        self.robo_planner.steps = self.__default_steps
        self._previous_table_data = None
        self._disable_path_updates = False

        top_bar = self.setup_top_bar_layout()
        top_bar.setAlignment(QtCore.Qt.AlignTop)

        joint_control_bar = self.setup_joint_controls_layout()
        table = self.setup_table_layout()
        controls = self.setup_running_controls_layout()

        layout = QtWidgets.QGridLayout()
        layout.addLayout(top_bar, 0, 0)
        self.joint_alignment = [1, 0, 1, 1, QtCore.Qt.AlignJustify]
        layout.addLayout(joint_control_bar, *self.joint_alignment)
        layout.addLayout(table, 2, 0)
        layout.addLayout(controls, 2, 1)

        self.setLayout(layout)

        self.robo_communicator.set_num_joints(self.w_num_joints.value())
        self.app_started = True

        self.table_updated()

    def setup_top_bar_layout(self):
        w_quit = QtWidgets.QPushButton('Quit')
        self.connect(w_quit, QtCore.SIGNAL('clicked()'), self.exit)

        self.w_port = BetterComboBox()
        self.rescan_ports()

        w_scan = QtWidgets.QPushButton('Scan')
        self.connect(w_scan, QtCore.SIGNAL('clicked()'), self.rescan_ports)

        self.w_con_discon = QtWidgets.QPushButton('Connect')
        self.connect(self.w_con_discon, QtCore.SIGNAL('clicked()'), self.con_discon)

        self.w_num_joints = QtWidgets.QSpinBox()
        self.w_num_joints.setRange(1, 9)
        self.w_num_joints.setValue(self.__default_num_joints)
        self.connect(self.w_num_joints, QtCore.SIGNAL('valueChanged(int)'), self.add_remove_joints)

        layout = QtWidgets.QHBoxLayout()
        layout.addWidget(w_quit)
        layout.addWidget(self.w_port)
        layout.addWidget(w_scan)
        layout.addWidget(self.w_con_discon)
        layout.addWidget(self.w_num_joints)

        return layout

    def setup_joint_controls_layout(self):
        layout = QtWidgets.QHBoxLayout()

        self.w_joint_controls = []
        num_joints = self.w_num_joints.value()
        for i in range(num_joints):
            w_joint_control = JointAngleSlider('Joint {}'.format(i))
            self.w_joint_controls.append(w_joint_control)
            self.connect(w_joint_control, QtCore.SIGNAL('valueChanged(double)'), self.joint_updated)
            layout.addWidget(w_joint_control)

        w_read_angles = QtWidgets.QPushButton('Read Angles')
        self.connect(w_read_angles, QtCore.SIGNAL('clicked()'), self.read_angles)
        layout.addWidget(w_read_angles)

        return layout

    def setup_table_layout(self):
        layout = QtWidgets.QGridLayout()

        w_up_bttn = QtWidgets.QPushButton('Delete Row')
        self.connect(w_up_bttn, QtCore.SIGNAL('clicked()'), self.delete_row)
        w_down_bttn = QtWidgets.QPushButton('Add Row')
        self.connect(w_down_bttn, QtCore.SIGNAL('clicked()'), self.add_row)

        self.w_table = QtWidgets.QTableWidget()
        self.set_table_data(self.__default_table_data)
        self.w_table.setCurrentCell(0, 0)
        self.connect(self.w_table, QtCore.SIGNAL('cellChanged(int, int)'), self.table_updated)
        self.connect(self.w_table, QtCore.SIGNAL('currentCellChanged(int, int, int, int)'), self.table_cell_changed)

        self.w_mpl_canvas = MplCanvas(self.robo_planner, width=5, height=4, dpi=100)

        layout.addWidget(w_up_bttn, 0, 0)
        layout.addWidget(w_down_bttn, 1, 0)
        layout.addWidget(self.w_table, 0, 1)
        layout.addWidget(self.w_mpl_canvas, 1, 1)

        return layout

    def setup_running_controls_layout(self):
        layout = QtWidgets.QGridLayout()

        w_force_update = QtWidgets.QPushButton('Force Update')
        self.connect(w_force_update, QtCore.SIGNAL('clicked()'), self.write_angles)
        self.w_auto_update = QtWidgets.QCheckBox('Auto Update')
        self.w_auto_update.setChecked(True)
        w_run = QtWidgets.QPushButton('Run')
        self.connect(w_run, QtCore.SIGNAL('clicked()'), self.run_program)
        self.w_path_type = BetterComboBox()
        self.w_path_type.setItems([v.value for v in PathType])
        self.connect(self.w_path_type, QtCore.SIGNAL('currentIndexChanged(int)'), self.table_updated)
        w_steps = QtWidgets.QSpinBox()
        w_steps.setRange(2, 1000)
        w_steps.setValue(self.__default_steps)
        self.connect(w_steps, QtCore.SIGNAL('valueChanged(int)'), self.change_steps)
        self.connect(w_steps, QtCore.SIGNAL('valueChanged(int)'), self.table_updated)
        w_optimize = QtWidgets.QPushButton('Optimize')
        self.connect(w_optimize, QtCore.SIGNAL('clicked()'), self.start_optimization)
        w_load_optimize = QtWidgets.QPushButton('Load from optimization')
        self.connect(w_load_optimize, QtCore.SIGNAL('clicked()'), self.read_optimization)
        w_save = QtWidgets.QPushButton('Save')
        self.connect(w_save, QtCore.SIGNAL('clicked()'), self.save_file)
        w_load = QtWidgets.QPushButton('Load')
        self.connect(w_load, QtCore.SIGNAL('clicked()'), self.load_file)

        layout.addWidget(w_force_update, 0, 0)
        layout.addWidget(self.w_auto_update, 0, 1)
        layout.addWidget(w_run, 1, 0)
        layout.addWidget(self.w_path_type, 1, 1)
        layout.addWidget(w_steps, 2, 0, 2, 1)
        layout.addWidget(w_optimize, 3, 0)
        layout.addWidget(w_load_optimize, 3, 1)
        layout.addWidget(w_load, 4, 0)
        layout.addWidget(w_save, 4, 1)

        return layout

    @QtCore.Slot()
    def start_optimization(self):
        self.table_updated()  # Re-load all paths
        self.robo_planner.optimize(cb=self.w_mpl_canvas.update_figure)
        self.w_mpl_canvas.update_figure()

    @QtCore.Slot()
    def read_optimization(self):
        t_data = self.get_table_data()

        optimized_times = [round(t, 5) for t in self.robo_planner.get_sanatized_time_path_diffs()]
        print(optimized_times)

        for i, t in enumerate(optimized_times):
            t_data[i][0] = t

        self.set_table_data(t_data)

    @QtCore.Slot()
    def joint_updated(self):
        angles = [j.value for j in self.w_joint_controls]

        t_data = self.get_table_data()
        curr_row = self.w_table.currentRow()
        t_data[curr_row] = [t_data[curr_row][0]] + angles
        self.set_table_data(t_data)

        if self.robo_communicator.is_connected() and self.w_auto_update.isChecked():
            self.robo_communicator.write_angles(angles)

    @QtCore.Slot()
    def table_cell_changed(self):
        t_data = self.get_table_data()
        curr_row = self.w_table.currentRow()
        angles = t_data[curr_row][1:]
        for i, j in enumerate(self.w_joint_controls):
            j.setValue(angles[i])
            j.update()

        if self.robo_communicator.is_connected():
            self.robo_communicator.write_angles(angles)

    @QtCore.Slot()
    def delete_row(self):
        t_data = self.get_table_data()
        curr_row = self.w_table.currentRow()

        del t_data[curr_row]

        if self.w_table.rowCount() > 1:
            self.w_table.setRowCount(self.w_table.rowCount() - 1)

        self.set_table_data(t_data)

    @QtCore.Slot()
    def add_row(self):
        self.w_table.setRowCount(self.w_table.rowCount() + 1)
        t_data = self.get_table_data()

        # import random
        # t_data[-1] = [random.randint(500, 2000) / 1000.0] + [random.randint(-90, 90) for i in range(len(t_data[0]) - 1)]
        t_data[-1] = t_data[-2]

        self.set_table_data(t_data)

    @QtCore.Slot()
    def rescan_ports(self):
        self.w_port.setItems(sorted(['%s' % p[0] for p in comports()]))

    @QtCore.Slot()
    def table_updated(self):
        if self._disable_path_updates or not self.app_started:
            return

        t_data = self.get_table_data()

        num_joints = self.w_num_joints.value()
        path_type = PathType(self.w_path_type.currentText())
        time_list = [0.0]
        for i in range(len(t_data) - 1):
            time_list.append(time_list[i] + t_data[i + 1][0])

        if len(time_list) <= 1:
            return

        joint_paths = []
        for i in range(num_joints):
            joint_path = []
            for row in t_data:
                joint_path.append(row[i + 1])
            joint_paths.append(joint_path)

        self.robo_planner.load_paths(time_list, joint_paths, path_type)
        self.w_mpl_canvas.update_figure()

    @QtCore.Slot()
    def run_program(self):
        if not self.robo_communicator.is_connected():
            return

        self.robo_planner.run(self.robo_communicator,
                              callback=lambda t: self.w_mpl_canvas.update_figure(cur_time=t))

    @QtCore.Slot()
    def con_discon(self):
        if self.robo_communicator.is_connected():
            self.robo_communicator.disconnect()
            self.w_con_discon.setText('Connect')
        else:
            com_port = self.w_port.currentText()

            try:
                self.robo_communicator.connect(com_port)
            except SerialException:
                return
            self.w_con_discon.setText('Disconnect')
            num_joints = int(self.robo_communicator.read_num_joints())
            print('Num Joints', num_joints)
            self.w_num_joints.setValue(num_joints)

    @QtCore.Slot(int)
    def change_steps(self, value):
        self.robo_planner.steps = value

    @QtCore.Slot()
    def save_file(self):
        filename = QtWidgets.QFileDialog.getSaveFileName(self, 'Save File', '', 'CSV Files (*.csv)')
        with open(filename[0], 'w') as f:
            table_data = self.get_table_data()
            for row in table_data:
                line = ','.join([str(r) for r in row]) + '\n'
                f.write(line)

    @QtCore.Slot()
    def load_file(self):
        filename = QtWidgets.QFileDialog.getOpenFileName(self, 'Open File', '', 'CSV Files (*.csv)')
        table_data = []
        with open(filename[0], 'r') as f:
            lines = f.readlines()
            for line in lines:
                row = [float(l) for l in line.split(',')]
                table_data.append(row)
        self.w_num_joints.setValue(len(table_data[0]) - 1)
        self.set_table_data(table_data)

    def get_table_data(self):
        num_rows = self.w_table.rowCount()
        num_cols = self.w_table.columnCount()
        table_data = []
        for r in range(num_rows):
            row_data = []
            for c in range(num_cols):
                t_item = self.w_table.item(r, c)
                if t_item is not None:
                    row_data.append(float(t_item.text()))
                else:
                    row_data.append(0.0)
            table_data.append(row_data)
        return table_data

    def set_table_data(self, table_data):
        self._disable_path_updates = True
        num_rows = len(table_data)
        num_cols = len(table_data[0])
        self.w_table.setRowCount(num_rows)
        self.w_table.setColumnCount(num_cols)
        for r in range(num_rows):
            for c in range(num_cols):
                self.w_table.setItem(r, c, QtWidgets.QTableWidgetItem(str(table_data[r][c])))
        self._disable_path_updates = False
        self.table_updated()

    @QtCore.Slot()
    def add_remove_joints(self):
        layout = self.layout()

        for L in range(layout.count()):  # Sketchy AF finding and deletion of layout
            l = layout.itemAt(L)
            if not isinstance(l.itemAt(0).widget(), JointAngleSlider):
                continue
            for i in range(l.count()):
                l.itemAt(i).widget().deleteLater()
            l.deleteLater()

        joint_control_bar = self.setup_joint_controls_layout()
        layout.addLayout(joint_control_bar, *self.joint_alignment)

        num_joints = self.w_num_joints.value()
        self.robo_communicator.set_num_joints(num_joints)
        self.w_table.setColumnCount(1 + num_joints)
        t_data = self.get_table_data()
        for row in range(len(t_data)):
            t_data[row][-1] = 0.0
        self.set_table_data(t_data)

    @QtCore.Slot()
    def write_angles(self):
        if not self.robo_communicator.is_connected():
            return

        angles = [j.value for j in self.w_joint_controls]
        self.robo_communicator.write_angles(angles)

    @QtCore.Slot()
    def read_angles(self):
        if not self.robo_communicator.is_connected():
            return
        angles = self.robo_communicator.read_angles()
        for i, j in enumerate(self.w_joint_controls):
            j.setValue(angles[i])
            j.update()

    @QtCore.Slot()
    def exit(self):
        if self.robo_communicator.is_connected():
            self.robo_communicator.disconnect()
        QtWidgets.qApp.quit()


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    board = MainWindow()
    board.setGeometry(100, 100, 2000, 1500)
    board.show()
    sys.exit(app.exec_())
