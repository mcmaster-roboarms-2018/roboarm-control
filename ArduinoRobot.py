import struct
import time
import traceback
import sys
import datetime
import math
import functools
import copy
from enum import Enum

import serial
from scipy import interpolate
import numpy as np

from PySide2 import QtCore


class SerConst(Enum):
    WRITE_ANGLES = int(0x0)
    READ_ANGLES = int(0x1)
    SYN = int(0x2)
    ACK = int(0x3)
    READ_NUM_JOINTS = int(0x5)


class RobotCommunicator(object):
    endian = '<'  # Arduino is little-endian

    def __init__(self):
        self.serial = None
        self.num_joints = None

    def connect(self, com_port):
        self.serial = serial.Serial(port=com_port,
                                    baudrate=115200,
                                    timeout=None)
        value = None
        while not value:
            value = self.serial.readline()
        self.send_cmd(SerConst.SYN)
        in_bytes = self.serial.read()
        if int.from_bytes(in_bytes, byteorder='little') != SerConst.ACK.value:
            print('WARNING: ACK ERROR {} != {}'.format(in_bytes, SerConst.ACK))
        return self

    def read_num_joints(self):
        if self.serial is None:
            raise serial.SerialException('Serial connection has not been defined yet')

        self.send_cmd(SerConst.READ_NUM_JOINTS)
        struct_fmt = 'B'
        in_bytes = self.serial.read(struct.calcsize(struct_fmt))
        num_joints = struct.unpack(self.endian + struct_fmt, in_bytes)[0]
        return num_joints

    def disconnect(self):
        self.serial.close()
        self.serial = None

    def is_connected(self):
        return self.serial is not None

    def set_num_joints(self, num_joints):
        self.num_joints = num_joints
        return self

    def write_angles(self, angles):
        """
        Takes in a list of angles to set the joints to.
        Communication protocol is a control byte, followed by signed integer bytes that represent joint angles.
        :param angles:
        """
        if self.serial is None:
            raise serial.SerialException('Serial connection has not been defined yet')

        if len(angles) != self.num_joints:
            raise IndexError('Number of arguments does not match number of joints {}'.format(angles))

        self.send_cmd(SerConst.WRITE_ANGLES)
        angles = [float(a) for a in angles]
        struct_fmt = 'f' * self.num_joints
        self.write_bytes(struct_fmt, *angles)

    def read_angles(self):
        if self.serial is None:
            raise serial.SerialException('Serial connection has not been defined yet')

        self.send_cmd(SerConst.READ_ANGLES)
        struct_fmt = 'f' * self.num_joints
        in_bytes = self.serial.read(struct.calcsize(struct_fmt))
        angles = struct.unpack(self.endian + struct_fmt, in_bytes)
        return angles

    def send_cmd(self, cmd):
        if self.serial is None:
            raise serial.SerialException('Serial connection has not been defined yet')

        self.write_bytes('B', cmd.value)
        in_byte = self.serial.read(1)
        rtn_cmd = struct.unpack('B', in_byte)
        if int.from_bytes(rtn_cmd, byteorder='little') != cmd.value:
            err_msg = self.serial.readline()
            print('Serial Error: {}'.format(err_msg))

    def write_bytes(self, fmt, *data):
        send_bytes = struct.pack(self.endian + fmt, *data)
        bytes_written = self.serial.write(send_bytes)
        self.serial.flush()
        return bytes_written


class PathType(Enum):
    LIN_PARABLEND = 'Linear ParaBlend'
    LINEAR = 'Linear'
    CUBIC = 'Cubic'
    RBF = 'RBF'
    PCHIP = 'PCHIP'


class JointPath(object):
    max_acceleration = 2000  # deg/s^2
    max_velocity = 540  # deg/s
    max_time_resolution = 10.0 * 10 ** -6
    slow_start_end_time = 0.3  # Seconds

    def __init__(self, steps, joint_space, time_space):
        self.steps = steps
        self.joint_space = joint_space  # List of points that need to be hit
        self.time_space = time_space  # List of times that correlate with the joint points
        self.final_joint_plan = []
        self.final_time_plan = []
        self.plot = []
        self.path_type = None

    def calculate_final_time_plan(self):
        time_steps = len(self.time_space)
        segment_length = round(self.steps / (time_steps - 1))
        self.final_time_plan = []
        for i in range(1, time_steps):
            self.final_time_plan += list(np.linspace(self.time_space[i - 1], self.time_space[i], segment_length))
        self.steps = len(self.final_time_plan)

    def plan_path(self, path_type):
        self.path_type = path_type
        self.calculate_final_time_plan()

        print('Before time space', self.time_space)

        selected_function = {
            PathType.LIN_PARABLEND: {'fn': self.plan_linear_parablend, 'args': [], 'kwargs': {}},
            PathType.LINEAR: {'fn': self.plan_generic_scipy, 'args': [self.joint_space, interpolate.interp1d], 'kwargs': {'kind': 'linear'}},
            PathType.CUBIC: {'fn': self.plan_generic_scipy, 'args': [self.joint_space, interpolate.CubicSpline], 'kwargs': {}},
            PathType.RBF: {'fn': self.plan_generic_scipy, 'args': [self.joint_space, interpolate.Rbf], 'kwargs': {}},
            PathType.PCHIP: {'fn': self.plan_generic_scipy, 'args': [self.joint_space, interpolate.PchipInterpolator], 'kwargs': {}},
        }[self.path_type]

        self.final_joint_plan = selected_function['fn'](*selected_function['args'], **selected_function['kwargs'])

        print('Path planned:', path_type)
        print('After time space', self.time_space)

    def optimize(self, cb=None):
        def move_time(time_list, at_point, t_diff):
            for i in range(at_point, len(time_list)):
                time_list[i] -= t_diff
            return time_list

        initial_time_movement = 0.5
        if self.path_type == PathType.LIN_PARABLEND:
            for i in range(2, len(self.time_space) - 1):
                current_time_movement = initial_time_movement
                while True:
                    original_time_space = copy.deepcopy(self.time_space)
                    original_joint_space = copy.deepcopy(self.joint_space)

                    self.time_space = move_time(self.time_space, i, current_time_movement)  # Modified path to try
                    self.calculate_final_time_plan()  # Recalculate final time plan
                    try:
                        self.final_joint_plan = self.plan_linear_parablend(max_velocity=self.max_velocity)  # Test the plan to see if it breaks
                    except Exception as e:
                        current_time_movement = current_time_movement / 2.0  # Try again with a smaller movement
                        if current_time_movement < self.max_time_resolution:
                            print('Err: ', e)
                            break  # If we're at max resolution, break
                        continue  # else try again
                    finally:
                        self.time_space = copy.deepcopy(original_time_space)  # Reset to the original
                        self.joint_space = copy.deepcopy(original_joint_space)

                    self.time_space = move_time(original_time_space, i, current_time_movement)  # Bake in the path
                    if cb is not None:
                        cb()
            print('Optimized time space:', self.time_space)
            return
        return

    def plan_generic_scipy(self, points, fn, **kwargs):
        num_points = len(points)
        x = np.arange(0, num_points)
        y = np.array(points)
        try:
            f = fn(x, y, **kwargs)
        except:
            return []
        xnew = np.linspace(0, num_points - 1, self.steps)
        interpolated_pts = f(xnew)
        return list(interpolated_pts)

    def add_slow_start_stop(self):
        # Need to modify the start and end points to have slow starts
        self.time_space = [t + self.slow_start_end_time for t in self.time_space]  # Shift all points
        self.time_space.insert(0, 0.0)  # Start
        self.time_space.append(self.time_space[-1] + self.slow_start_end_time)  # End

        self.joint_space.insert(0, self.joint_space[0])  # Start
        self.joint_space.append(self.joint_space[-1])  # End

    def plan_linear_parablend(self, max_velocity=None):
        """
        Will output a list of points with straight segments at maximum acceleration connected by parabolas
        :return:
        """
        self.add_slow_start_stop()
        self.calculate_final_time_plan()  # Recalculate final time plan

        points = list(map(list, zip(self.time_space, self.joint_space)))  # (time, angle)
        t_d = lambda a, b: points[b][0] - points[a][0]
        t = [None] * len(points)
        a = [None] * len(points)
        v = [None] * (len(points) - 1)  # nth element is v_n_n-1
        t_ab = [None] * (len(points) - 1)  # nth element is t_n_n-1

        # Compute first and last acceleration
        a[0] = (1 if points[1][1] > points[0][1] else -1) * self.max_acceleration
        a[-1] = (1 if points[-2][1] > points[-1][1] else -1) * self.max_acceleration

        # Compute first t
        t[0] = t_d(0, 1) - math.sqrt(t_d(0, 1) ** 2 - ((2 * (points[1][1] - points[0][1])) / a[0]))

        # Compute first v from t[0]
        v[0] = (points[1][1] - points[0][1])/(t_d(0, 1) - (0.5 * t[0]))

        # Compute inside v's
        for j in range(1, len(points) - 1):
            k = j + 1
            v[j] = (points[k][1] - points[j][1]) / t_d(j, k)  # v_j_k

        # Compute last t from last a
        t[-1] = t_d(-2, -1) - math.sqrt(t_d(-2, -1) ** 2 + ((2 * (points[-1][1] - points[-2][1])) / a[-1]))

        # Compute last v from last t
        try:
            v[-1] = (points[-1][1] - points[-2][1]) / (t_d(-2, -1) - (0.5 * t[-1]))
        except ZeroDivisionError:
            v[-1] = 0.0

        # Compute inside a's from two things
        for j in range(1, len(points) - 1):
            k = j + 1
            a[j] = (1 if v[j] > v[j - 1] else -1) * self.max_acceleration

        # Compute inside t's from three things
        for j in range(1, len(points) - 1):
            k = j + 1
            t[j] = (v[j] - v[j - 1]) / a[j]

        # Compute first t_ab from inside t and first t
        t_ab[0] = t_d(0, 1) - t[0] - (0.5 * t[1])

        # Compute last t_ab from two things
        t_ab[-1] = t_d(-2, -1) - t[-1] - (0.5 * t[-2])

        # Compute the inside t_ab's
        for j in range(1, len(points) - 1):
            k = j + 1
            t_ab[j] = t_d(j, k) - 0.5 * t[j] - 0.5 * t[k]

        if max_velocity is not None and True in [(i if i > 0 else -i) > max_velocity for i in v]:
            raise Exception('Maximum velocity exceded')

        def between(a, b):
            return lambda X: (eval('%s' % a) <= X) * (X <= eval('%s' % b))

        func_list = []
        cond_list = []
        self.plot = []
        # First parabola and straight line
        # Line
        a_0 = v[0]
        a_1 = points[0][1] - v[0] * points[0][0]
        s_eq = functools.partial(lambda a_0, a_1, x: a_0 * x + a_1, a_0, a_1)

        C = (points[1][0] - (t[1] / 2), s_eq(points[1][0] - (t[1] / 2)))  # To the left of the next point
        B = (points[0][0] + (t[0] / 2), s_eq(points[0][0] + (t[0] / 2)))  # To the right of the point
        A = (0, points[0][1])  # Flat line at start
        if max_velocity is not None and B[0] > C[0]:
            raise Exception('There is no longer a straight component')

        self.plot += [A, B, C]

        if A[0] == B[0]:  # Flat line
            s_cond = between(A[0], C[0])  # Extend the line
            cond_list += [s_cond]
            func_list += [s_eq]
        else:
            s_cond = between(B[0], C[0])

            # Parabola
            a_0 = 0.5 * a[0]
            a_1 = (a_0 * (B[0] ** 2 - A[0] ** 2) + A[1] - B[1]) / (A[0] - B[0])
            a_2 = (a_0 * A[0] * B[0] * (A[0] - B[0]) + A[0] * B[1] - B[0] * A[1]) / (A[0] - B[0])

            p_eq = functools.partial(lambda a_0, a_1, a_2, x: a_0 * x ** 2 + a_1 * x + a_2, a_0, a_1, a_2)
            p_cond = between(A[0], B[0])

            cond_list += [p_cond, s_cond]
            func_list += [p_eq, s_eq]

        # Inner parabolas and straight lines
        for i in range(1, len(points) - 1):
            j = i + 1
            # Line
            a_0 = v[i]
            a_1 = points[i][1] - v[i] * points[i][0]
            s_eq = functools.partial(lambda a_0, a_1, x: a_0 * x + a_1, a_0, a_1)

            C = (points[j][0] - (t[j] / 2), s_eq(points[j][0] - (t[j] / 2)))  # To the left of the next point
            B = (points[i][0] + (t[i] / 2), s_eq(points[i][0] + (t[i] / 2)))  # To the right of the point
            A = (points[i][0] - (t[i] / 2), func_list[-1](points[i][0] - (t[i] / 2)))  # To the left of the point
            if max_velocity is not None and B[0] > C[0]:
                raise Exception('There is no longer a straight component')

            self.plot += [A, B, C]
            if A[0] == B[0]:  # Flat line
                s_cond = between(A[0], C[0])  # Extend the line
                cond_list += [s_cond]
                func_list += [s_eq]
                # print('point %i has only flat' % i)
            else:
                s_cond = between(B[0], C[0])

                # Parabola
                a_0 = 0.5 * a[i]
                a_1 = (a_0 * (B[0] ** 2 - A[0] ** 2) + A[1] - B[1]) / (A[0] - B[0])
                a_2 = (a_0 * A[0] * B[0] * (A[0] - B[0]) + A[0] * B[1] - B[0] * A[1]) / (A[0] - B[0])

                p_eq = functools.partial(lambda a_0, a_1, a_2, x: a_0 * x ** 2 + a_1 * x + a_2, a_0, a_1, a_2)
                p_cond = between(A[0], B[0])

                cond_list += [p_cond, s_cond]
                func_list += [p_eq, s_eq]

        interpolated_pts = []
        for T in self.final_time_plan:
            point = None
            for I, cond in enumerate(cond_list):
                if cond(T):
                    point = func_list[I](T)
            if point is None:
                point = None
                print('%s cannot be evaluated' % T)
            interpolated_pts.append(point)

        return interpolated_pts


class Worker(QtCore.QRunnable):
    """
    Worker thread

    Inherits from QRunnable to handler worker thread setup, signals and wrap-up.

    :param callback: The function callback to run on this worker thread. Supplied args and
                     kwargs will be passed through to the runner.
    :type callback: function
    :param args: Arguments to pass to the callback function
    :param kwargs: Keywords to pass to the callback function

    """
    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()

        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignals()
        self.kwargs['progress_callback'] = self.signals.progress

    @QtCore.Slot()
    def run(self):
        """
        Initialise the runner function with passed args, kwargs.
        """

        # Retrieve args/kwargs here; and fire processing using them
        try:
            result = self.fn(*self.args, **self.kwargs)
        except Exception:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        self.signals.finished.emit()


class WorkerSignals(QtCore.QObject):
    error = QtCore.Signal(tuple)
    progress = QtCore.Signal(float)
    finished = QtCore.Signal()


class RobotPlanner(object):
    def __init__(self, steps):
        self.joint_paths = []
        self.steps = steps
        self.threadpool = QtCore.QThreadPool()

    def load_paths(self, time_list, joint_paths, path_type):
        """
        Load joint paths
        :param time_list: List times corresponding to joint points
        :param joint_paths: List of lists containing all joint paths
        """
        self.joint_paths = []
        for joint_path in joint_paths:
            if len(time_list) != len(joint_path):
                raise TypeError('Length of time and joint lists do not match {} != {}'.format(len(time_list), len(joint_path)))
            self.joint_paths.append(JointPath(self.steps, joint_path, time_list))

        self.plan_paths(path_type)

        return self

    def get_sanatized_time_path_diffs(self):
        """
        Useful after optimization
        :return: The first joint's time space.
        """
        sanatized_time_space = copy.deepcopy(self.joint_paths[0].time_space)
        slow_start_end_time = self.joint_paths[0].slow_start_end_time

        sanatized_time_space.pop(-1)   # End
        sanatized_time_space.pop(0)  # Start
        sanatized_time_space = [t - slow_start_end_time for t in sanatized_time_space]  # Shift all points

        time_space_diff = [sanatized_time_space[0]]
        cur_sum = time_space_diff[0]
        for t in sanatized_time_space[1:]:
            time_space_diff.append(t - cur_sum)
            cur_sum += time_space_diff[-1]

        return time_space_diff

    def optimize(self, cb=None):
        for i, j in enumerate(self.joint_paths):
            print('Optimizing joint %i' % i)
            j.optimize(cb)

    def plan_paths(self, path_type):
        for j in self.joint_paths:
            j.plan_path(path_type)

    def run(self, robot_coms, callback=None):
        # Pass the function to execute
        worker = Worker(self.run_path, robot_coms, progress_callback=callback)  # Any other args, kwargs are passed to the run function
        worker.signals.progress.connect(callback)

        self.threadpool.start(worker)

    def run_path(self, robot_coms, progress_callback=None):
        """
        This function should be run as a thread, as it is blocking
        """
        def TimestampMillisec64():
            return int((datetime.datetime.utcnow() - datetime.datetime(1970, 1, 1)).total_seconds() * 1000)

        time_list = self.joint_paths[0].final_time_plan  # Just use index 0 as a timekeeper

        start_time = TimestampMillisec64()
        for i, t in enumerate(time_list):
            angles = []
            for j in self.joint_paths:
                angles.append(j.final_joint_plan[i])
            robot_coms.write_angles(angles)
            if progress_callback is not None:
                progress_callback.emit(t)

            if i + 1 < len(time_list):
                end_time = TimestampMillisec64()
                sleep_time = time_list[i + 1] - ((end_time - start_time) / 1000.0)
                if sleep_time > 0:
                    time.sleep(sleep_time)
                else:
                    print('{}: late by {}'.format(i, round(-sleep_time, 5)))
