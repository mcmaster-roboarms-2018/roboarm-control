from PySide2 import QtCore, QtWidgets
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.animation as animation
from matplotlib.figure import Figure


class BetterComboBox(QtWidgets.QComboBox):
    def setItems(self, items):
        for i in range(self.count()):
            self.removeItem(i)
        self.addItems(items)


class FPQdial(QtWidgets.QDial):
    valueChanged = QtCore.Signal(float)
    factor = 100.0

    def __init__(self):
        QtWidgets.QDial.__init__(self)

        self.connect(self, QtCore.SIGNAL('valueChanged(int)'), self, QtCore.SLOT('valueChangedConverter(int)'))

    def setRange(self, min, max):
        QtWidgets.QDial.setRange(self, min * self.factor, max * self.factor)

    @QtCore.Slot(float)
    def setValue(self, value):
        QtWidgets.QDial.setValue(self, int(round(value * self.factor)))

    @QtCore.Slot(int)
    def valueChangedConverter(self, val):
        val = float(val) / self.factor
        self.valueChanged.emit(val)


class JointAngleSlider(QtWidgets.QWidget):
    valueChanged = QtCore.Signal(float)
    min_value = -90
    max_value = 90
    value = 0

    def __init__(self, text_label):
        QtWidgets.QWidget.__init__(self)

        self.slider = FPQdial()
        self.slider.setRange(self.min_value, self.max_value)
        self.slider.setValue((self.max_value + self.min_value) / 2)
        self.connect(self.slider, QtCore.SIGNAL('valueChanged(double)'), self, QtCore.SIGNAL('valueChanged(double)'))

        self.spinbox = QtWidgets.QDoubleSpinBox()
        self.spinbox.setRange(self.min_value, self.max_value)
        self.connect(self.spinbox, QtCore.SIGNAL('valueChanged(double)'), self, QtCore.SIGNAL('valueChanged(double)'))

        self.connect(self.slider, QtCore.SIGNAL('valueChanged(double)'), self.spinbox, QtCore.SLOT('setValue(double)'))
        self.connect(self.spinbox, QtCore.SIGNAL('valueChanged(double)'), self.slider, QtCore.SLOT('setValue(double)'))
        self.connect(self, QtCore.SIGNAL('valueChanged(double)'), self.setValue)

        self.label = QtWidgets.QLabel()
        self.label.setText(text_label)
        self.label.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop)
        self.label.setSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.spinbox)
        layout.addWidget(self.slider)
        layout.addWidget(self.label)
        self.setLayout(layout)

        self.setFocusProxy(self.slider)

    @QtCore.Slot(float)
    def setValue(self, value):
        self.value = value
        self.spinbox.setValue(value)
        self.slider.setValue(value)


class MplCanvas(FigureCanvas):
    def __init__(self, robo_planner, width=5, height=4, dpi=100):
        self.robo_planner = robo_planner
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)

        FigureCanvas.__init__(self, fig)

        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        self.ani = animation.FuncAnimation(fig, self.animate, interval=100, blit=True)
        self.cur_time = 0.0
        self.artists = []
        self.animated_line = None

    def update_figure(self, cur_time=None):
        if len(self.robo_planner.joint_paths) == 0:
            return

        if cur_time is not None:  # Only updating the time line, leave to annimation
            self.cur_time = cur_time
            return

        self.axes.cla()

        self.artists = []
        for i, j in enumerate(self.robo_planner.joint_paths):
            rough_x = j.time_space
            smooth_x = j.final_time_plan
            try:
                self.artists += [
                    self.axes.scatter(rough_x, j.joint_space),
                    self.axes.plot(smooth_x, j.final_joint_plan, label='Joint {}'.format(i))[0],
                ]
            except ValueError as e:
                print('Plotting error:', e)
        try:
            x = [p[0] for p in self.robo_planner.joint_paths[0].plot]
            y = [p[1] for p in self.robo_planner.joint_paths[0].plot]
            self.artists += [self.axes.scatter(x, y)]
        except Exception as e:
            print(e)
        self.axes.legend()
        self.draw()
        self.repaint()

    def animate(self, _):
        self.animated_line = self.axes.axvline(x=self.cur_time)
        return self.artists + [self.animated_line]

